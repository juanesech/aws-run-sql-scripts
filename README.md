Run sql script on MySQL Aurora instance, getting credentials from AWS Secrets Manager

### Pre-Requisites:
* python-mysqldb


### Use:

```bash
python main.py "name-of-cloudformation-export" "secret-name" "scripts-directory"
```