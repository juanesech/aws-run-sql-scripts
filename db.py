#!/usr/bin/env python3
import MySQLdb
import os


def run_db_scripts(host, user, password, scripts_path):
    print("Connecting to DB")
    db = MySQLdb.connect(host=host,
                         user=user,
                         passwd=password,
                         port=3306)

    try:
        db.cursor()
    except Exception as err:
        print("Err: ", err)

    cur = db.cursor()

    scripts = os.listdir(scripts_path)
    for script in scripts:
        print("Running: "+script)
        fd = open(scripts_path + "/" + script, 'r')
        sql_file = fd.read()
        fd.close()

        try:
            cur.execute(sql_file)
        except Exception as msg:
            print("Script fail: ", msg)
        else:
            print("Success: "+script)

    db.close()
