#!/usr/bin/env python3
import boto3
import json

client = boto3.client('secretsmanager')


class Credentials:
    def __init__(self, secret_id):
        secret = client.get_secret_value(SecretId=secret_id)
        self.user = json.loads(secret['SecretString'])['AuroraDBUser']
        self.password = json.loads(secret['SecretString'])['AuroraDBPassword']
