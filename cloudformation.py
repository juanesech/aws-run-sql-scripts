#!/usr/bin/env python3
import boto3

client = boto3.client('cloudformation')


def get_aurora_endpoint(export_name):
    next_token = client.list_exports()['NextToken']
    endpoint = ""
    while next_token:
        for item in client.list_exports(NextToken=next_token)['Exports']:
            if item['Name'] == export_name:
                endpoint = item['Value']
        try:
            client.list_exports(NextToken=next_token)['NextToken']
        except Exception:
            break
        else:
            next_token = client.list_exports(NextToken=next_token)['NextToken']
    return endpoint
