#!/usr/bin/env python3

from db import *
from secrets import *
from cloudformation import *
import sys

app_stack_export = sys.argv[1]
secret_name = sys.argv[2]
scripts_path = sys.argv[3]

endpoint = get_aurora_endpoint(app_stack_export)
credentials = Credentials(secret_name)
run_db_scripts(endpoint, credentials.user, credentials.password, scripts_path)
